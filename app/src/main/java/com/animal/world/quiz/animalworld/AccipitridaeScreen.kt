package com.animal.world.quiz.animalworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.animal.world.quiz.animalworld.R
import com.animal.world.quiz.animalworld.databinding.ActivityAccipitridaeScreenBinding

class AccipitridaeScreen : AppCompatActivity() {

    private lateinit var binding: ActivityAccipitridaeScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccipitridaeScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var score = intent.getIntExtra("score", 0)

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.green))
            score++
            binding.button2.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.red))
            binding.button1.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
        }

        Log.d("AccipScreenScore", "$score ")


        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, LepidopteraScreen::class.java)
            intent.putExtra("score", score)
            startActivity(intent)
            finish()
        }

        binding.hamb.setOnClickListener {
            finish()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        moveTaskToBack(false)
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

}