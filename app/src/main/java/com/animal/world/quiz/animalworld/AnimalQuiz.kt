package com.animal.world.quiz.animalworld

data class AnimalQuiz(
    val title: String,
    val image: Int,
    val option1: String,
    val option2: String,
    val option3: String,
    val press1: Boolean,
    val press2: Boolean,
    val press3: Boolean
)