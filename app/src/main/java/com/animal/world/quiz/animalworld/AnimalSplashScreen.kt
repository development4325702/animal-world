package com.animal.world.quiz.animalworld

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import com.animal.world.quiz.animalworld.databinding.ActivityAnimalSplashScreenBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@SuppressLint("CustomSplashScreen")
class AnimalSplashScreen : AppCompatActivity() {

    private lateinit var binding: ActivityAnimalSplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAnimalSplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        GlobalScope.launch {
            delay(3100)
            val intent = Intent(this@AnimalSplashScreen, MainScreen::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}