package com.animal.world.quiz.animalworld

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.animal.world.quiz.animalworld.databinding.ActivityFishScreenBinding

class FishScreen : AppCompatActivity() {
    private lateinit var binding: ActivityFishScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fish_screen)

        binding = ActivityFishScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var score = intent.getIntExtra("score", 0)

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.green))
            binding.button1.isClickable = false
            binding.button3.isClickable = false
            score++
        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, SinaScreen::class.java)
            intent.putExtra("score", score)
            startActivity(intent)
            finish()
        }

        binding.hamb.setOnClickListener {
            finish()
        }

    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}