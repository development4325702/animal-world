package com.animal.world.quiz.animalworld
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import com.animal.world.quiz.animalworld.databinding.ActivityLepidopteraScreenBinding

class LepidopteraScreen : AppCompatActivity() {

    private lateinit var binding: ActivityLepidopteraScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLepidopteraScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var score = intent.getIntExtra("score", 0)

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.red))
            binding.button1.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.green))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
            score++
        }

        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, ElephantsScreen::class.java)
            intent.putExtra("score", score)
            startActivity(intent)
            finish()
        }

        binding.hamb.setOnClickListener {
            finish()
        }

    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

}