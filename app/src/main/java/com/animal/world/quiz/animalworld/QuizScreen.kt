package com.animal.world.quiz.animalworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.animal.world.quiz.animalworld.FoxScreen
import com.animal.world.quiz.animalworld.R
import com.animal.world.quiz.animalworld.databinding.ActivityQuizScreenBinding

class QuizScreen : AppCompatActivity() {

    private lateinit var binding: ActivityQuizScreenBinding

    var correctAnswer = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuizScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.green))
            correctAnswer++
            binding.button1.isClickable = false
            binding.button3.isClickable = false
        }

        binding.button3.setOnClickListener {
            binding.button3.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
            binding.button1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            val intent = Intent(this, FoxScreen::class.java)
            intent.putExtra("score", correctAnswer)
            startActivity(intent)
            finish()
        }

        binding.hamb.setOnClickListener {
            finish()
        }

    }


    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}