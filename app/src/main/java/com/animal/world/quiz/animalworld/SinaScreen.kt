package com.animal.world.quiz.animalworld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.animal.world.quiz.animalworld.databinding.ActivitySinaScreenBinding

class SinaScreen : AppCompatActivity() {
    private lateinit var binding: ActivitySinaScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySinaScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setFullScreen()

        var score = intent.getIntExtra("score", 0)

        binding.button1.setOnClickListener {
            binding.button1.setBackgroundColor(resources.getColor(R.color.red))
            binding.button2.isClickable = false
        }

        binding.button2.setOnClickListener {
            binding.button2.setBackgroundColor(resources.getColor(R.color.green))
            score++
            binding.button1.isClickable = false
        }

        binding.nextBtn.setOnClickListener {
            binding.resultLayout.visibility = View.VISIBLE
            binding.scoreValue.text = score.toString()
            binding.nextBtn.text = "to menu"
            binding.nextBtn.setOnClickListener {
                finish()
            }
            binding.mainConstraint.setOnClickListener {
                binding.resultLayout.visibility = View.GONE
            }
        }

        binding.hamb.setOnClickListener {
            finish()
        }

    }

    private fun setFullScreen() {
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}